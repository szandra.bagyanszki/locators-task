using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LocatorsTest
{
    public class TesterClass
    {
        IWebDriver driver;

        public IWebDriver Driver
        {
            get
            { return driver; }
            set
            {
                driver.Quit();
                driver = value;
            }
        }

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
        }

        [TearDown]
        protected void Teardown()
        {
            driver.Quit();
        }


        [Test]
        public void Test1()
        {
            Driver.Url = "https://www.bbc.com/sport";

            //Id
            var footerContent = Driver.FindElements(By.Id("footer-content"));
            Assert.That(footerContent.Count, Is.EqualTo(1));

            var footerContentWithXPathAxes = Driver.FindElements(By.XPath("//*[contains(@class, 'ssrcss-1wnvszv-FooterStack')]/*/*"));
            Assert.That(footerContentWithXPathAxes.Count, Is.EqualTo(1));

            //Name
            var metaName = Driver.FindElement(By.Name("description"));
            StringAssert.Contains("sports", metaName.GetAttribute("content"));

            var metaNameWithXPathAxes = Driver.FindElement(By.XPath("//title/following::meta"));
            StringAssert.AreEqualIgnoringCase("Sports news and live sports coverage including scores, results, video, audio and analysis on Football, F1, Cricket, Rugby Union and all other UK sports.", metaNameWithXPathAxes.GetAttribute("content"));

            //TagName
            var tables = Driver.FindElements(By.TagName("table"));
            Assert.That(tables.Count, Is.EqualTo(0));

            var tablesWithXPathAxes = Driver.FindElements(By.XPath("//descendant::table"));
            Assert.That(tablesWithXPathAxes.Count, Is.EqualTo(0));

            //ClassName
            var legalLink = Driver.FindElement(By.ClassName("ssrcss-1xssqxs-Link-LegalLink")); //finds more than one with this classname, returns the first
            StringAssert.Contains("terms", legalLink.GetAttribute("href"));

            var legalLinkWithXPathAxes = Driver.FindElement(By.XPath("//*[@class='ssrcss-x3nlsb-ClusterItems e1ihwmse0']/li[1]/a"));
            StringAssert.Contains("terms", legalLinkWithXPathAxes.GetAttribute("href"));

            //Linktext
            var cookiesLink = Driver.FindElement(By.LinkText("Cookies"));
            StringAssert.AreEqualIgnoringCase("https://www.bbc.com/usingthebbc/cookies", cookiesLink.GetAttribute("href"));

            var cookiesLinkWithXPathAxes = Driver.FindElement(By.XPath("//*[@class='ssrcss-x3nlsb-ClusterItems e1ihwmse0']/descendant::a[4]"));
            StringAssert.AreEqualIgnoringCase("https://www.bbc.com/usingthebbc/cookies", cookiesLinkWithXPathAxes.GetAttribute("href"));

            //PartialLinkText
            var contactLink = Driver.FindElement(By.PartialLinkText("Contact"));
            StringAssert.Contains("contact", contactLink.GetAttribute("href"));

            var contactLinkLinkWithXPathAxes = Driver.FindElement(By.XPath("//*[@class='ssrcss-1xssqxs-Link-LegalLink e1nhpvv71']/../following-sibling::li[6]/a"));
            StringAssert.Contains("contact", contactLinkLinkWithXPathAxes.GetAttribute("href"));

            //CssSelector
            var yellowFootballLink = Driver.FindElement(By.CssSelector("a[href='/sport/football'].ssrcss-1ec1ytn-StyledLink"));
            Assert.That(yellowFootballLink.GetCssValue("display"), Is.EqualTo("block"));

            var yellowFootballLinkWithXPathAxes = Driver.FindElement(By.XPath("//*[contains(@class, 'enbola60')]/preceding-sibling::div[1]/ul/li[2]/a"));
            Assert.That(yellowFootballLinkWithXPathAxes.GetCssValue("display"), Is.EqualTo("block"));

            //XPath
            var searchLinkDiv = Driver.FindElement(By.XPath("//*[contains(@class, 'ssrcss-s2g7dp-NavigationLink-SearchLink')]"));
            Assert.That(searchLinkDiv.GetAttribute("href"), Is.EqualTo("https://www.bbc.co.uk/search?d=SPORT_GNL"));

            var searchLinkDivWithXPathAxes = Driver.FindElement(By.XPath("//div[attribute::aria-label='Search BBC']/child::*"));
            Assert.That(searchLinkDivWithXPathAxes.GetAttribute("href"), Is.EqualTo("https://www.bbc.co.uk/search?d=SPORT_GNL"));
        }
    }
}